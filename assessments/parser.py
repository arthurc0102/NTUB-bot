from core.parser import BruteParser


class GetListParser(BruteParser):
    """
    抓取教學評量列表

    HTML 的 id 說明：
        ASMList: 課程列表表格

        ASMList_ctl02_OpClass: 開課班級
        ASMList_ctl02_CosId: 科目代碼
        ASMList_ctl02_CosName: 科目名稱
        ASMList_ctl02_Tch: 授課教師
        P.S. 以上四個的 02 取代為項次
    """
    def __init__(self):
        super().__init__()
        self.can_data = False
        self.this_class = []
        self.all_class = []
        self.target_id_start = 'ASMList_ctl'
        self.target_id_end_list = [
            '_OpClass',
            '_CosId',
            '_CosName',
            '_Tch',
        ]
        self.in_p_tag = False
        self.in_a_tag = False
        self.a_attrs = []

    def handle_starttag(self, tag, attrs):
        super().handle_starttag(tag, attrs)
        for key, value in attrs:
            if key == 'id':
                try:
                    if value[:11] != self.target_id_start:
                        continue

                    if value[13:] in self.target_id_end_list:
                        self.can_data = True

                except Exception:
                    # id length to short
                    continue

        if tag == 'p':
            self.in_p_tag = True
        elif tag == 'a':
            self.in_a_tag = True
            self.a_attrs = attrs

    def handle_data(self, data):
        if self.can_data:
            self.this_class.append(data)
            self.can_data = False

        if self.in_a_tag and data == '填寫':
            for key, value in self.a_attrs:
                if key == 'onclick':
                    # value example: ShowDetail('105','2','50640A','42','五專資管四甲','0570','陳怡如','英文(四)(下)','','1');
                    # remove last two chars, remove first 11 chars and remove "'"
                    self.this_class.append(value[11:len(value) - 3].replace('\'', ''))
                    break
        elif self.in_p_tag and data == '已填寫':
            self.this_class.append(False)

        if len(self.this_class) == 5:
                self.all_class.append(self.this_class)
                self.this_class = []

    def handle_endtag(self, tag):
        if tag == 'p':
            self.in_p_tag = False
        elif tag == 'a':
            self.in_a_tag = False
            self.a_attrs = []


class DetailParser(BruteParser):
    def __init__(self):
        super().__init__()
        self.question_count = 0

    def handle_starttag(self, tag, attrs):
        super().handle_starttag(tag, attrs)
        for key, value in attrs:
            if key == 'name' and value == 'queNo':
                self.question_count += 1
