import requests

from django.shortcuts import render, redirect
from django.contrib import messages
from django.http import JsonResponse

from .parser import GetListParser, BruteParser, DetailParser

# urls
base_url = 'http://ntcbadm.ntub.edu.tw/STDWEB/'
class_list_url = base_url + 'Assessment_Main.aspx'
detail_url = base_url + 'Assessment_Detail.aspx'
detail_ta_url = base_url + 'Assessment_Detail_TA.aspx'

# error message
not_time_error_message = '目前非期末教學評量時間'
login_error_message = '閒置過久或尚未登入系統，請重新登入系統!'


def index(request):
    cookies = request.session.get('cookies')
    req = requests.Session()
    res = req.get(class_list_url, cookies=cookies)
    if is_res_error(request, res):
        return redirect('users:logout')

    parser = GetListParser()
    parser.feed(res.text)

    all_class_parameter_string = ''
    for c in parser.all_class:
        if c[-1]:
            # not fill yet class
            all_class_parameter_string += c[-1] + ';'

    return render(request, 'assessments/index.html', {
        'class': parser.all_class,
        'all_class_parameter_string': all_class_parameter_string,
        'name': request.session.get('name', ''),
        'std_id': request.session.get('std_id', ''),
    })


def fill(request):
    if request.method == 'GET':
        return redirect('users:login')

    success = fill_assessments(
        request,
        request.POST.get('parametersString'),
        request.POST.get('score'),
        request.session.get('cookies'),
    )

    msg = messages.get_messages(request)
    return JsonResponse({
        'success': success,
        'messages': [m.message for m in msg]
    })


def fill_all(request):
    if request.method == 'GET':
        return redirect('users:login')

    score = request.POST.get('score')
    cookies = request.session.get('cookies')
    for parametersString in request.POST.get('parametersStrings').split(';'):
        fill_assessments(request, parametersString, score, cookies)

    msg = messages.get_messages(request)
    return JsonResponse({
        'messages': [m.message for m in msg]
    })


# functions


def is_res_error(request, res):
    flag = False
    if res.status_code != 200:
        messages.add_message(request, messages.INFO, '有些東西錯誤了，請重新嘗試')
        flag = True
    elif login_error_message in res.text:
        messages.add_message(request, messages.INFO, '登入失敗，請重新登入')
        flag = True
    elif not_time_error_message in res.text:
        messages.add_message(request, messages.INFO, '現在非教學評量填寫時間')
        flag = True

    return flag


def fill_assessments(request, parameters_string, score, cookies):
    req = requests.Session()
    res = req.get(class_list_url, cookies=cookies)
    if is_res_error(request, res):
        return False

    parameter_keys = [
        'Hide_Years',
        'Hide_Term',
        'Hide_OpClass',
        'Hide_Serial',
        'Hide_ClassShort',
        'Hide_TchNo',
        'Hide_TchName',
        'Hide_Cos_Name',
        'Hide_TermType',
        'Hide_SelStyle'
    ]
    parameter_values = parameters_string.split(',')

    parser = BruteParser()
    parser.feed(res.text)
    # parameter_values don't need last
    parser.post_data.update(dict(zip(parameter_keys, parameter_values[:-1])))

    # Decision detail url
    target_url = detail_ta_url if parameter_values[-1] == '3' else detail_url

    # Get detail page
    res = req.post(target_url, data=parser.post_data, cookies=cookies)

    parser = DetailParser()
    parser.feed(res.text)

    result = ','.join(['{}-{}-'.format(i, score)
                       for i in range(1, parser.question_count + 1)])

    parser.post_data.update({
        'SaveData': 'Y',
        'Hide_Str': result,
        'Hide_Str2': '3<|>Hello World',
    })

    res = req.post(target_url, data=parser.post_data, cookies=cookies)
    return res.status_code == 200 and res.url == class_list_url
