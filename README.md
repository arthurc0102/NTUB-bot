# NTUB Bot

> 北商大教學評量機器人

## 分支說明

- [master branch](https://github.com/arthurc0102/ntubbot) 為使用 Django 框架的版本
- [flask-version branch](https://github.com/arthurc0102/ntubbot/tree/flask-version) 為使用 Flask 框架的版本
- [script-version branch](https://github.com/arthurc0102/ntubbot/tree/script-version) 為非網頁的單機版
- [gh-pages branch](https://github.com/arthurc0102/ntubbot/tree/gh-pages) 為目標網頁 (測試用)

## 使用方式

- 建立虛擬環境：`virtualenv env`

- 執行虛擬環境：`source ./env/bin/active`

- 安裝套件：`pip install -r requirements.txt`

- 開啟 debug 模式
    - Linux or Mac: `export DEBUG=True`
    - Window: `set DEBUG=True`
    
- 建立 secret key
    1. 將 [secret.example.py](./core/settings/secret.example.py) 複製成 `secret.py`
    2. 產生 secret key，[方法](https://github.com/arthurc0102/Note/tree/master/Python/Django/secret-key-gen)
    3. 貼到檔案 `secret.py` 中
    
- 建立資料表：`python manage.py migrate`

- 啟動：`python manage.py runserver`
