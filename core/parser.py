from html.parser import HTMLParser


class BruteParser(HTMLParser):
    def __init__(self):
        super().__init__()

        self.post_data = {}

    def handle_starttag(self, tag, attrs):
        if tag == 'input':
            tag_name = None
            tag_value = None
            for key, value in attrs:
                if key == 'name':
                    tag_name = value

                elif key == 'value':
                    tag_value = value

            if tag_name is not None and tag_value is not None:
                self.post_data.update({tag_name: tag_value})
