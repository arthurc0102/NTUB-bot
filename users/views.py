import requests

from django.shortcuts import render, redirect
from django.contrib import messages

from core.parser import BruteParser

from .parser import GetNameParser


# url
login_url = 'http://ntcbadm.ntub.edu.tw/'
home_url = 'http://ntcbadm.ntub.edu.tw/Portal/indexSTD.aspx'

# error message
login_error_message = '閒置過久或尚未登入系統，請重新登入系統!'


def login(request):
    # check have login before or not
    cookies = request.session.get('cookies')
    if cookies is not None:
        res = requests.get(home_url, cookies=cookies)
        if login_error_message not in res.text:
            return redirect('assessments:index')

    if request.POST:
        req = requests.Session()
        parser = BruteParser()
        res = req.get(login_url)
        if res.status_code == 200:
            parser.feed(res.text)
            parser.post_data.update({
                'UserID': request.POST.get('username'),
                'PWD': request.POST.get('password'),
            })
            res = req.post(login_url, data=parser.post_data)
            if res.status_code == 200 and res.url == home_url:
                parser = GetNameParser()
                parser.feed(res.text)
                request.session['cookies'] = dict(req.cookies)
                request.session['std_id'] = request.POST.get('username', '')
                request.session['name'] = parser.name
                return redirect('assessments:index')

        messages.add_message(request, messages.INFO, '登入失敗，請重新嘗試')

    return render(request, 'users/login.html', {'username': request.POST.get('username', '')})


def logout(request):
    if request.session.get('cookies') is not None:
        del request.session['cookies']
        del request.session['std_id']
        del request.session['name']

    return redirect('users:login')
