from html.parser import HTMLParser


class GetNameParser(HTMLParser):
    def __init__(self):
        super().__init__()
        self.in_target = False
        self.name = ''

    def handle_starttag(self, tag, attrs):
        if tag != 'span':
            return

        for key, value in attrs:
            if key == 'id' and value == 'Name':
                self.in_target = True

    def handle_data(self, data):
        if self.in_target:
            self.name = data
            self.in_target = False
